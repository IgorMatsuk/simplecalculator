package calc;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private TextField main, top;

    @FXML
    private Button two;

    @FXML
    private Button three;

    @FXML
    private Button six;

    @FXML
    private Button five;

    @FXML
    private Button four;

    @FXML
    private Button seven;

    @FXML
    private Button eight;

    @FXML
    private Button nine;

    @FXML
    private Button equals;

    @FXML
    private Button zero;

    @FXML
    private Button clear;

    @FXML
    private Button plus;

    @FXML
    private Button mult;

    @FXML
    private Button minus;

    @FXML
    private Button one;

    @FXML
    private Button division;

    private int operator ;
    double a,b,result ;
    String prevStr="";
    @FXML
    private void handleButtonAction(ActionEvent event) {
        String str = ((Labeled)event.getSource()).getText();
        main.setText(main.getText()+str);
        top.setText(prevStr+str);
        prevStr = top.getText();
    }

    @FXML
    private void add(){
        try{
            a = Double.parseDouble(main.getText());
            operator = 1 ;
            top.setText(prevStr+"+");
            prevStr = top.getText();
        }catch(NumberFormatException e){
            System.out.println("Enter value First");
        }finally{
            main.setText("");
        }
    }

    @FXML
    private void substract(){
        try{
            a = Double.parseDouble(main.getText());
            operator = 2 ;
            top.setText(prevStr+"-");
            prevStr = top.getText();
        }catch(NumberFormatException e){
            System.out.println("Enter value First");
        }finally{
            main.setText("");
        }
    }

    @FXML
    private void multiply(){
        try{
            a = Double.parseDouble(main.getText());
            operator = 3 ;
            top.setText(prevStr+"*");
            prevStr = top.getText();
        }catch(NumberFormatException e){
            System.out.println("Enter value First");
        }finally{
            main.setText("");
        }
    }
    @FXML
    private void clearText(){
        main.setText("");
        top.setText("");
        prevStr="";
    }

    @FXML
    private void divide(){
        try{
            a = Double.parseDouble(main.getText());
            operator = 4 ;
            top.setText(prevStr+"/");
            prevStr = top.getText();
        }catch(NumberFormatException e){
            System.out.println("Enter value First");
        }finally{
            main.setText("");
        }
    }

    @FXML
    private void equals(){
        try{
            b = Double.parseDouble(main.getText());

            switch(operator){
                case 1 : result = a + b ; main.setText(""+result);  break;
                case 2 : result = a - b ; main.setText(""+result);  break;
                case 3 : result = a * b ; main.setText(""+result);  break;
                case 4 : try{
                    result = a/b ;
                    main.setText(""+result);

                }catch(Exception e){
                    main.setText("Error");
                }
            }
            top.setText(prevStr+"="+result);



        }catch(NumberFormatException e){
            System.out.println("Select values First");
        }
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}